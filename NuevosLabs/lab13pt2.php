<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="style13pt2.css">

    <title>Hello, world!</title>
  </head>
  <body>



<?php require("sesionActivaLab.php") ?> 





<header>
     <div class="container">
       <?php
       	echo "<h1>BIENVENIDO :". $_SESSION['usuario']."</h1><br><br><br";
        ?>
      
     </div>

   </header>



<div class="container">
  
      <section class="main row">

         <aside class="col-xs-12 col-sm-12  col-lg-12 col-md-12">
           <h3> Sube una Imagen  </h3>
           <div class="container">
            <form  class="form-group" action="upload.php" method="post" enctype="multipart/form-data"> 
            	<input type="file" name="file"><br>
            	<input type="submit" name="enviar" value="Subir Imagen">
            	
            </form>


            <p >
              <a href="cierre_sesionLab.php">Cerrar Sesion</a>
            </p>
        </div>
         </aside>
     </section>


     <div class="row">
       
      <div id="color2" class=" col-xs-12 col-sm-12 col-md-12">
        <h2><strong>Preguntas </strong></h2>
    	<h3>
       ¿Por qué es importante hacer un session_unset() y luego un session_destroy()? 
      </h3>

      <p>
        session_unset() por si solo no es suficiente, solamente libera la sesion para uso sin embargo la sesion sigue en la computadora, por lo que se tiene que destruir completamente la sesion o sesiones existentes, session_destroy() libera la variable $_SESSION, entonces el ponerlos juntos asegura que la sesion quede completamente libre. 
      </p>


      <h3> ¿Cuál es la diferencia entre una variable de sesión y una cookie?</h3>


      <p> La diferencia es que una cookie son bits de datos que se almacenan por el navegador y son mandados al servidor con todas las peticiones. Una sesion es una colleccion de datos almacenados en el servidor y asociados con un usuario en particular. </p>
      </div>


      <h3>¿Qué técnicas se utilizan en sitios como facebook para que el usuario no sobreescriba sus fotos en el sistema de archivos cuando sube una foto con el mismo nombre?</h3>

      <p>Se utilizan muchas tecnicas, que involucran desde la restriccion de ciertos formatos para subir las imagenes, ahora se utiliza tambien el File Hash, lo que hace esto es lo siguiente. el modulo de File Hash, genera y almacena MD5, SHA-1 y/o SHA-256 hashes para cada archivo subidio al sitio. Los valores de Hash son cargados en el objeto del archivo donde estan disponibles para el tema y los demas modulos, Controladores son proporcionados para la compatibilidad y vista. Por otra parte a los hashes completos se le proporcionan tokens para ser util en el contenido. </p>

      <h3>¿Qué es CSRF y cómo puede prevenirse?</h3>

      <p> CSRF es un tipo de exploit malicioso de un sitio web en el que comandos no autorizados son transmitidos por un usuario en el cual el sitio web confia. Esta vulnerabilidad es conocida ctambien por otros nombres como XSRF, enlaces hostil, ataque de un click.<br>
      Lo primero que hay que hacer es un identificador aleatorio por cada pedido, una cadena única que se genera para cada sesión. Generamos el identificador y lo incluimos en cada formulario como una entrada oculta. El sistema revisará si el formulario es válido, comparando el identificador con el que está almacenado en la variable de sesión del usuario. O sea que para que un atacante pueda generar un pedido, deberá conocer el valor del identificador.<br>  El segundo método es usar un nombre aleatorio para cada campo del formulario. El valor del nombre aleatorio de cada campo se almacena en una variable de sesión y el sistema generará un nuevo valor aleatorio después de que se haya enviado el formulario. O sea que para que funcione un ataque, el atacante tendrá que adivinar estos nombres aleatorios en los formularios.</p>
</div>


<footer >
 <div class="container-fluid">
    <h3> </h3>
 </div>
</footer>










    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>