SET DATEFORMAT DMY

-- 1 La suma de las cantidades e importe total de todas las entregas realizadas durante el 97.
SELECT SUM(CANTIDAD)  AS 'Total Cantidad', SUM((M.COSTO+M.COSTO*M.PORCENTAJEIMPUESTO/100)*CANTIDAD) AS 'IMPORTE FINAL'
FROM MATERIALES M, ENTREGAN E
WHERE E.CLAVE=M.CLAVE AND E.FECHA BETWEEN '01/01/1997' AND '31/12/1997'

--2 Para cada proveedor, obtener la raz�n social del proveedor, n�mero de entregas e importe total de las entregas realizadas. 
SELECT P.RAZONSOCIAL, COUNT(E.RFC) AS 'Entregas (numero Total)', 
SUM((M.COSTO+M.COSTO*M.PORCENTAJEIMPUESTO/100)*CANTIDAD) AS 'Total Importe'
FROM MATERIALES M, ENTREGAN E, PROVEEDORES P
WHERE E.CLAVE=M.CLAVE AND P.RFC =E.RFC
GROUP BY P.RAZONSOCIAL

--3 Por cada material obtener la clave y descripci�n del material, la cantidad total entregada, la m�nima cantidad entregada, la m�xima cantidad entregada, el importe total de las entregas de aquellos materiales en los que la cantidad promedio entregada sea mayor a 400. 
SELECT M.CLAVE, M.DESCRIPCION, SUM(CANTIDAD) AS 'Total Cantidad', MIN(CANTIDAD) AS 'Minimo',
MAX(CANTIDAD) AS 'Maximo', (M.COSTO+M.COSTO*M.PORCENTAJEIMPUESTO/100)*SUM(CANTIDAD) AS 'Total Importe'
FROM MATERIALES M, ENTREGAN E
WHERE M.CLAVE=E.CLAVE
GROUP BY M.CLAVE, M.DESCRIPCION,M.COSTO,M.PORCENTAJEIMPUESTO
HAVING AVG(CANTIDAD)>400

--4 Para cada proveedor, indicar su raz�n social y mostrar la cantidad promedio de cada material entregado, detallando la clave y descripci�n del material, excluyendo aquellos proveedores para los que la cantidad promedio sea menor a 500. 

SELECT P.RAZONSOCIAL,M.CLAVE,M.DESCRIPCION, AVG(CANTIDAD) AS ' PROMEDIO (Cantidad)'
FROM PROVEEDORES P, ENTREGAN E,MATERIALES M
WHERE E.RFC=P.RFC AND M.CLAVE=E.CLAVE
GROUP BY P.RAZONSOCIAL,M.CLAVE,M.DESCRIPCION
HAVING AVG(CANTIDAD)<500

--5 Mostrar en una solo consulta los mismos datos que en la consulta anterior pero para dos grupos de proveedores: aquellos para los que la cantidad promedio entregada es menor a 370 y aquellos para los que la cantidad promedio entregada sea mayor a 450. 
SELECT P.RAZONSOCIAL,M.CLAVE,M.DESCRIPCION, AVG(CANTIDAD) AS ' PROMEDIO (Cantidad)'
FROM PROVEEDORES P, ENTREGAN E,MATERIALES M
WHERE E.RFC=P.RFC AND M.CLAVE=E.CLAVE
GROUP BY P.RAZONSOCIAL,M.CLAVE,M.DESCRIPCION
HAVING AVG(CANTIDAD)<370 OR AVG(CANTIDAD)>450
ORDER BY ' PROMEDIO (Cantidad)'

SELECT * FROM MATERIALES

INSERT INTO MATERIALES VALUES('1440','Pintura B2004','125','1')
INSERT INTO MATERIALES VALUES('1450','Tepetate rojo','40','1.1')
INSERT INTO MATERIALES VALUES('1460','Pintura C0032','125','1.2')
INSERT INTO MATERIALES VALUES('1470','Pintura C11002','125','1.3')
INSERT INTO MATERIALES VALUES('1480','Pintura N10012','125','1.4')


-- 6 Clave y descripci�n de los materiales que nunca han sido entregados. 
SELECT CLAVE, DESCRIPCION
FROM MATERIALES
WHERE CLAVE NOT IN(SELECT CLAVE FROM ENTREGAN)

-- 7 Raz�n social de los proveedores que han realizado entregas tanto al proyecto 'Vamos M�xico' como al proyecto 'Quer�taro Limpio'. 

SELECT DISTINCT P.RAZONSOCIAL
FROM PROVEEDORES P, ENTREGAN E, PROYECTOS PR
WHERE E.RFC=P.RFC  AND PR.NUMERO=E.NUMERO AND PR.DENOMINACION='VAMOS MEXICO'
AND P.RAZONSOCIAL IN (SELECT P.RAZONSOCIAL
FROM PROVEEDORES P, ENTREGAN E, PROYECTOS PR
WHERE E.RFC=P.RFC AND PR.NUMERO=E.NUMERO AND PR.DENOMINACION='QUERETARO LIMPIO')

--8 Descripci�n de los materiales que nunca han sido entregados al proyecto 'CIT Yucat�n'. 
SELECT DESCRIPCION
FROM MATERIALES
WHERE CLAVE NOT IN (SELECT CLAVE FROM ENTREGAN E, PROYECTOS P
					WHERE P.NUMERO=E.NUMERO AND P.DENOMINACION='CIT YUCATAN')


-- 9 Raz�n social y promedio de cantidad entregada de los proveedores cuyo promedio de cantidad entregada es mayor al promedio de la cantidad entregada por el proveedor con el RFC 'VAGO780901'. 
SELECT RAZONSOCIAL, AVG(CANTIDAD) AS 'Promedio(Cantidad)'
FROM PROVEEDORES P, ENTREGAN E
WHERE E.RFC=P.RFC
GROUP BY RAZONSOCIAL
HAVING AVG(CANTIDAD) > (SELECT AVG(CANTIDAD) FROM ENTREGAN
						WHERE RFC='VAGO780901')


						
--10 RFC, raz�n social de los proveedores que participaron en el proyecto 'Infonavit Durango' y cuyas cantidades totales entregadas en el 2000 fueron mayores a las cantidades totales entregadas en el 2001. 

SET DATEFORMAT DMY
SELECT P.RFC, RAZONSOCIAL
FROM PROVEEDORES P, ENTREGAN E, PROYECTOS PR
WHERE E.RFC= P.RFC AND PR.NUMERO=E.NUMERO
AND PR.DENOMINACION='INFONAVIT DURANGO' AND FECHA BETWEEN
'01/01/2000' AND '31/12/2000'
GROUP BY P.RFC, RAZONSOCIAL
HAVING SUM(CANTIDAD)>(SELECT SUM(CANTIDAD) FROM PROVEEDORES P, ENTREGAN E, PROYECTOS PR
						WHERE E.RFC= P.RFC AND PR.NUMERO=E.NUMERO
						AND PR.DENOMINACION='INFONAVIT DURANGO' AND FECHA BETWEEN
						'01/01/2001' AND '31/12/2001')