<!DOCTYPE html>
<html>
<head>
	<title> LAB 20 </title>
	  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="colores.css">
<link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet"> 
</head>
<body>

	<div class="container">

<h1> LISTA DE USUARIOS  </h1>

	<button  class="btn btn-success" onclick="mostrarUsuarios()"> MOSTRAR </button><br><br>
</div>

	<div id="info" class="container"></div>


	<div id="info" class="container" >
		
		<h2> Preguntas  a responder </h2>
		<br><br>


		<h4 id="preg1"> Explica y elabora un diagrama sobre cómo funciona AJAX con jQuery.</h4>
			<button id="ver1" class="btn btn-default"> Respuesta </button><br><br>
			<p id="resp1"> <img src="image1.jpg"> <br> Con Ajax podremos hacer peticiones HTTP (GET y POST) sin necesidad de recargar la página con la consiguiente mejora de rendimiento y menor uso de ancho de banda, ya que sólo pedimos lo que necesitamos y lo cargamos dinámicamente en nuestra web.<br>

En este caso las vamos a realizar con Jquery, ya que nos simplifica la tarea.  Jquery dispone de muchos métodos para realizar las peticiones uno de ellos y el mas generico es:  $.Ajax(). A este método podemos pasarle distintos parámetros según nuestras necesidades, algunos de los más comunes son los siguientes:<br>

    Url: indica la url donde enviaremos la petición.<br>
    Type: especifica el tipo de petición (GET o POST).<br>
    Data: datos que enviamos junto a la petición.<br>
    DataType: tipo de datos que esperamos en la repuesta del servidor (XML, HTML, JSON, SCRIPT).<br>
    Async: Para elegir s queremos una petición síncrona o asíncrona.<br>

Una vez terminada la petición Ajax, tenemos 3 métodos con los que podemos recoger los datos recibidos, o controlar si ha habido algún error, o simplemente hacer algo cuando termine la petición.<br>

.done: Si la petición Ajax se ha realizado correctamente, entra en este método. En data están los datos enviados por el servidor.<br>

.fail: Se ejecuta si ha ocurrido algún problema en la petición, por ejemplo cuando la url especificada no existe.<br>

.always: Como su nombre indica, se ejecuta siempre, independientemente si la petición es correcta o no. Muy útil si por ejemplo queremos quitar una imagen o texto indicando que se están cargando datos.<br></p>


		<h4 id="preg2"> ¿Qué alternativas a jQuery existen?</h4>
			<button id="ver2" class="btn btn-default"> Respuesta </button><br><br>
			<p id="resp2"> Prototype, Ext, Dojo Toolkit, MooTools, Scrip.aculo.us , kilebarrow/chibi, Sencha Ext JS , entre otros...</p>

		<h4 id="preg3"> Componentes JQUERY utilizados</h4>
			<button id="ver3" class="btn btn-default"> Respuesta </button><br><br>
			<p id="resp3"> En este laboratorio se utilizaron varios componentes de JQUERY , para las preguntas y respuestas se utilizo un script en donde se llama a la funcion .click y dependiendo de este se llama a la funcion fadeIn para mostrar las respuestas. Tambien se utilizo AJAX en donde se hizo la llamada a la base de datos y aqui se mostraron los usuarios existentes en las tablas.  </p>
	</div>



<script type="text/javascript" src ="ajax2.js"></script>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>


<script >
	

	$(document).ready(function(){

		$("#resp1").hide();
		$("#resp2").hide();
		$("#resp3").hide();

		$("#ver1").click(function(){

					$("#resp2").hide();
					$("#resp3").hide();
					$("#resp1").fadeIn(700);


		});


		$("#ver2").click(function(){
			$("#resp1").hide();
			$("#resp3").hide();

			$("#resp2").fadeIn(700);


		});


				$("#ver3").click(function(){
							$("#resp1").hide();
							$("#resp2").hide();

							$("#resp3").fadeIn(700);


		});


	});


</script>



</body>
</html>