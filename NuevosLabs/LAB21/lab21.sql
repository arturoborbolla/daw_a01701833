
            IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaMaterial' AND type = 'P')
                DROP PROCEDURE creaMaterial
            GO
            
            CREATE PROCEDURE creaMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
            GO



			-- EXECUTE 

			EXECUTE creaMaterial 5000,'Martillos Acme',250,15
			-- VERIFICAR 
			SELECT * FROM MATERIALES

-- modificaMaterial 


            IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'modificaMaterial' AND type = 'P')
                DROP PROCEDURE modificaMaterial
            GO
            
            CREATE PROCEDURE modificaMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
				UPDATE MATERIALES SET  DESCRIPCION = @udescripcion, COSTO = @ucosto, PorcentajeImpuesto = @uimpuesto WHERE CLAVE = @uclave 
                
            GO

						-- EXECUTE modificaMaterial

			EXECUTE modificaMaterial 5000,'Martillos LAB',250,15
			-- VERIFICAR 
			SELECT * FROM MATERIALES


	IF EXISTS(SELECT name FROM sysobjects WHERE name = 'eliminaMaterial' AND type ='P')
		DROP PROCEDURE eliminaMaterial 
		GO 

		CREATE PROCEDURE eliminaMaterial
			@uclave NUMERIC(5,0)

		AS 
			DELETE FROM MATERIALES WHERE CLAVE = @uclave 
		GO
			-- EXECUTE ELIMINA MATERIAL 
			EXECUTE eliminaMaterial 5000

		-- VERIFICA ELIMINAR 
			SELECT * FROM MATERIALES


---------- TABLA PROYECTOS 


            IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaProyecto' AND type = 'P')
                DROP PROCEDURE creaProyecto
            GO
            
            CREATE PROCEDURE creaProyecto
                @unumero NUMERIC(5,0),
                @udenominacion VARCHAR(50)

            AS
                INSERT INTO PROYECTOS VALUES(@unumero,@udenominacion)
            GO



			-- EXECUTE 

			EXECUTE creaProyecto 6000,'infonavit Queretaro'
			-- VERIFICAR 
			SELECT * FROM PROYECTOS

-- modificaMaterial 


            IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'modificaProyecto' AND type = 'P')
                DROP PROCEDURE modificaProyecto
            GO
            
            CREATE PROCEDURE modificaProyecto
                @unumero NUMERIC(5,0),
                @udenominacion VARCHAR(50)
            AS
				UPDATE PROYECTOS SET  DENOMINACION = @udenominacion WHERE NUMERO = @unumero
            GO

						-- EXECUTE modificaMaterial

			EXECUTE modificaProyecto 5010,'infonavit Guerrero'
			-- VERIFICAR 
			SELECT * FROM PROYECTOS


	IF EXISTS(SELECT name FROM sysobjects WHERE name = 'eliminaProyecto' AND type ='P')
		DROP PROCEDURE eliminaProyecto 
		GO 

		CREATE PROCEDURE eliminaProyecto
			 @unumero NUMERIC(5,0)

		AS 
			DELETE FROM PROYECTOS WHERE NUMERO = @unumero
		GO
			-- EXECUTE ELIMINA MATERIAL 
			EXECUTE eliminaProyecto 5010

		-- VERIFICA ELIMINAR 
		
			SELECT * FROM PROYECTOS

------ TABLA PROVEEDORES 


            IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaProveedor' AND type = 'P')
                DROP PROCEDURE creaProveedor
            GO
            
            CREATE PROCEDURE creaProveedor
                @uRFC VARCHAR(13),
                @uRazonSocial VARCHAR(50)

            AS
                INSERT INTO PROVEEDORES VALUES(@uRFC,@uRazonSocial)
            GO



			-- EXECUTE 

			EXECUTE creaProveedor aaaa7876786321,'NUEVO RFC'
			-- VERIFICAR 
			SELECT * FROM PROVEEDORES

-- modificaMaterial 


            IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'modificaProveedor' AND type = 'P')
                DROP PROCEDURE modificaProveedor
            GO
            
            CREATE PROCEDURE modificaProveedor
                @uRFC VARCHAR(13),
                @uRazonSocial VARCHAR(50)
            AS
				UPDATE PROVEEDORES SET  RAZONSOCIAL = @uRazonSocial WHERE RFC = @uRFC
            GO

						-- EXECUTE modificaMaterial

			EXECUTE modificaProveedor aaaa7876786321 , 'ECOGERR'
			-- VERIFICAR 
			SELECT * FROM PROVEEDORES


	IF EXISTS(SELECT name FROM sysobjects WHERE name = 'eliminaProveedor' AND type ='P')
		DROP PROCEDURE eliminaProveedor
		GO 

		CREATE PROCEDURE eliminaProveedor
			 @uRFC VARCHAR(13)

		AS 
			DELETE FROM PROVEEDORES WHERE RFC = @uRFC
		GO
			-- EXECUTE ELIMINA MATERIAL 
			EXECUTE eliminaProveedor aaaa7876786321

		-- VERIFICA ELIMINAR 
		
			SELECT * FROM PROVEEDORES




---------- TABLA ENTREGAN 

	



            IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaEntregan' AND type = 'P')
                DROP PROCEDURE creaEntregan
            GO
            
            CREATE PROCEDURE creaEntregan
				@uclave NUMERIC(5,0),
                @uRFC VARCHAR(13),
				@uNumero NUMERIC(5,0),
				@uFecha DATETIME,
				@uCantidad NUMERIC(8,2)
                

            AS
                INSERT INTO ENTREGAN VALUES(@uclave,@uRFC,@uNumero,@uFecha,@uCantidad)
            GO



			-- EXECUTE 
			SET DATEFORMAT dmy
			EXECUTE creaEntregan 4000, AAAA800101, 5010, SYSDATETIME(), 6
			-- VERIFICAR 
			SELECT * FROM ENTREGAN

-- modifica ENTREGAN 


            IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'modificaEntregan' AND type = 'P')
                DROP PROCEDURE modificaEntregan
            GO
            
            CREATE PROCEDURE modificaEntregan
				@uclave NUMERIC(5,0),
                @uRFC VARCHAR(13),
				@uNumero NUMERIC(5,0),
				@uFecha DATETIME,
				@uCantidad NUMERIC(8,2)
            AS
				UPDATE ENTREGAN SET  CANTIDAD = @uCantidad WHERE  CLAVE = @uclave AND RFC = @uRFC AND NUMERO = @uNumero AND FECHA = @uFecha
            GO

						-- EXECUTE MODIFICA ENTREGAN

			EXECUTE modificaEntregan 4000, AAAA800101, 5010, 1997,6 
			-- VERIFICAR 
			SELECT * FROM ENTREGAN


	IF EXISTS(SELECT name FROM sysobjects WHERE name = 'eliminaEntregan' AND type ='P')
		DROP PROCEDURE eliminaEntregan 
		GO 

		CREATE PROCEDURE eliminaEntregan
				@uclave NUMERIC(5,0),
                @uRFC VARCHAR(13),
				@uNumero NUMERIC(5,0),
				@uFecha DATETIME


		AS 
			DELETE FROM ENTREGAN  WHERE  CLAVE = @uclave AND RFC = @uRFC AND NUMERO = @uNumero AND FECHA = @uFecha
		GO
			-- EXECUTE ELIMINA MATERIAL 
			EXECUTE eliminaEntregan  4000,aaaa7876786321,2000,1997

		-- VERIFICA ELIMINAR 
		
			SELECT * FROM ENTREGAN 



----------------------------------------------------------------------------------

----------------- QUERY 


                            IF EXISTS (SELECT name FROM sysobjects 
                                       WHERE name = 'queryMaterial' AND type = 'P')
                                DROP PROCEDURE queryMaterial
                            GO
                            
                            CREATE PROCEDURE queryMaterial
                                @udescripcion VARCHAR(50),
                                @ucosto NUMERIC(8,2)
                            
                            AS
                                SELECT * FROM Materiales WHERE descripcion 
                                LIKE '%'+@udescripcion+'%' AND costo > @ucosto 

								GO

								EXECUTE queryMaterial 'Lad', 20